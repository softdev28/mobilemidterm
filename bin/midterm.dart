import 'dart:io';

import 'package:midterm/midterm.dart' as midterm;

void main(List<String> arguments) {
  String? string = stdin.readLineSync();
  List<String>? list = string?.split(" ");
  List<String> postfix = toPostfix(list!);

  print(EvaluatePostfix(postfix));
}

List<String> toPostfix(List<String> list) {
  List<String> operators = [];
  List<String> postfix = [];
  list.forEach((token) {
    if (int.tryParse(token) != null) {
      postfix.add(token);
    }
    if (isOperator(token)) {
      while (!operators.isEmpty &&
          operators.last != "(" &&
          precedence(token) <= precedence(operators.last)) {
        postfix.add(operators.removeLast());
      }
      operators.add(token);
    }
    if (token == "(") {
      operators.add(token);
    }
    if (token == ")") {
      while (operators.last != "(") {
        postfix.add(operators.removeLast());
      }
      operators.removeLast();
    }
  });

  while (!operators.isEmpty) {
    postfix.add(operators.removeLast());
  }

  return postfix;
}

bool isOperator(String token) {
  switch (token) {
    case "+":
      return true;
    case "-":
      return true;
    case "*":
      return true;
    case "/":
      return true;
  }
  return false;
}

int precedence(String token) {
  switch (token) {
    case "+":
      return 1;
    case "-":
      return 1;
    case "*":
      return 2;
    case "/":
      return 2;
    case "(":
      return 3;
  }
  return -1;
}

double EvaluatePostfix(List<String> postfix) {
  List<double> values = [];
  postfix.forEach((token) {
    if (int.tryParse(token) != null) {
      values.add(double.parse(token));
    } else {
      double right = values.removeLast();
      double left = values.removeLast();
      switch (token) {
        case "+":
          values.add(left + right);
          break;
        case "-":
          values.add(left - right);
          break;
        case "*":
          values.add(left * right);
          break;
        case "/":
          values.add(left / right);
          break;
        default:
          break;
      }
    }
  });

  return values.first;
}
